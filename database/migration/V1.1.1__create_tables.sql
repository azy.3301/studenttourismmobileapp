drop table if exists users cascade;
drop table if exists users_university cascade;
drop table if exists university cascade;
drop table if exists city cascade;
drop table if exists lab cascade;
drop table if exists room cascade;
drop table if exists events cascade;
drop table if exists achievments cascade;
drop table if exists users_to_events cascade;


create table users(
    id int generated always as identity primary key,
    email text,
    uid text,
    phone int,
    vk text,
    first_name text,
    last_name text,
    patronymic text,
    role text,
    gender text,
    date_of_birth date,
    city text,
    university text,
    Specialization text,
    Second_specialization text,
    image_icon text
);

create table users_university(
    id int generated always as identity primary key,
    email text,
    password text,
    phone int,
    vk text,
    first_name text,
    last_name text,
    patronymic text,
    role text,
    gender text,
    date_of_birth date,
    city text,
    university text,
    Specialization text,
    Second_specialization text
);

create table university(
    id int generated always as identity primary key,
    users_university_id int not null references users_university,
    title text,
    description text,
    address text,
    image_main text
);

create table city(
    id int generated always as identity primary key,
    name text,
    foundation_date date,
    image_main text,
    image_gallery text
);

create table lab(
    id int generated always as identity primary key,
    title text,
    description text,
    type text,
    link text,
    image_main text,
    image_gallery text
);

create table room(
    id int generated always as identity primary key,
    university_id int not null references university,
    number_residents int,
    title text,
    description text,
    address text,
    price int,
    food boolean,
    min_time_days int,
    max_time_days int
);

create table events(
    id int generated always as identity primary key,
    start_date Date,
    finish_date Date,
    title text,
    description text,
    image_main text,
    contact_name text,
    contact_phone int,
    contact_email text
);

create table achievments(
    id int generated always as identity primary key,
    image_main text,
    contact_name text,
    contact_phone int,
    contact_email text
);

create table users_to_events(
    users_id  int not null references users,
    events_id int not null references events
);