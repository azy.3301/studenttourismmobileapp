from flask_app import app
from flask_app.routers.user import edit, get_achievement, get_events, add_events, add_trip, delete, \
    delete_trip, get_trip
from flask_app.routers.globals import events, news, labs, achievements, universities, rooms

if __name__ == "__main__":
    app.run(debug=True)
