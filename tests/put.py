import requests
from settings import test_id
import os

headers = {"token": test_id}
url = "http://127.0.0.1:5000/user/edit"

resp = requests.put(url, headers=headers)
print(resp.text)
