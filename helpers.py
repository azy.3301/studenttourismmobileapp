import firebase_admin
from firebase_admin import auth, credentials

cred = credentials.Certificate("cred.json")
firebase_admin.initialize_app(cred)


def get_user(token):
    return auth.verify_id_token(token)
