FROM python:3.9.13-alpine3.16

WORKDIR /flask_app

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY app.py .

ENV FLASK_APP=app.py


