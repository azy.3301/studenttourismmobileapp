import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final bool enabled;
  final String? hint;
  final String? label;
  final Function(String) updValue;
  final Widget? prefix;
  final Widget? suffix;
  final Either<TextEditingController, String> initial;
  const CustomTextField({
    Key? key,
    this.enabled = true,
    required this.updValue,
    required this.initial,
    this.hint,
    this.label,
    this.prefix,
    this.suffix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController? controller;
    String? initialValue;
    initial.fold(
      (ctrl) => controller = ctrl,
      (val) => initialValue = val,
    );

    return Container(
      padding: label != null ? const EdgeInsets.only(top: 12) : null,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondaryContainer,
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: TextFormField(
        enabled: enabled,
        controller: controller,
        initialValue: initialValue,
        onChanged: updValue,
        decoration: InputDecoration(
          labelText: label,
          prefixIcon: prefix,
          suffixIcon: suffix,
          hintText: hint,
          contentPadding: const EdgeInsets.only(top: 0, left: 20, right: 20, bottom: 5),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }
}
