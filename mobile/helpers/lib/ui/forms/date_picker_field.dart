import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:helpers/ui/forms/custom_text_field.dart';

import '../../logic/entity/entity.dart';

class DatePickerField<V extends Value> extends StatelessWidget {
  final bool enabled;
  final DateTime current;
  // This should trigger an update higher in the widget tree
  final Function(DateTime) updValue;
  final String label;
  final String Function(DateTime) format;
  final DateTime first;
  final DateTime last;
  const DatePickerField({
    Key? key,
    required this.enabled,
    required this.updValue,
    required this.current,
    required this.label,
    required this.format,
    required this.first,
    required this.last,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: !enabled
          ? null
          : () async {
              final chosenDate = await showDatePicker(
                context: context,
                initialDate: current,
                firstDate: first,
                lastDate: last,
              );
              if (chosenDate != null) {
                print(chosenDate);
                updValue(chosenDate);
              }
            },
      child: CustomTextField(
        // TODO: get rid of this duct-taped solution
        key: UniqueKey(),
        enabled: false,
        label: "Input your $label",
        hint: label,
        updValue: (_) {},
        initial: Right(format(current)),
      ),
    );
  }
}
