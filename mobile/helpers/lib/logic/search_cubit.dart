import 'package:bloc/bloc.dart';

typedef SearchState = String;

class SearchCubit extends Cubit<SearchState> {
  SearchCubit() : super("");

  void updSearchText(SearchState searchText) => emit(searchText.toLowerCase());
}
