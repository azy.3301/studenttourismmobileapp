import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class UnauthenticatedException implements Exception {}

class EmailState extends Equatable {
  final String? email;
  final bool isVerified;
  @override
  List get props => [email, isVerified];
  const EmailState(this.email, this.isVerified);
}

class UserState extends Equatable {
  final String id;
  final String? displayName;
  final EmailState emailState;
  final String token;
  @override
  List get props => [id, displayName, emailState, token];
  const UserState({
    required this.id,
    required this.displayName,
    required this.emailState,
    required this.token,
  });
}

typedef AuthState = Option<UserState>;

abstract class AuthFacade {
  Future<Either<Exception, void>> refresh();
  Stream<AuthState> getStateStream();
  Future<AuthState> getState();
  Future<UserState> forceGetState();
  Future<Either<Exception, void>> updateDisplayName(String displayName);
  Future<Either<Exception, void>> updateEmail(String email);
  Future<Either<Exception, void>> verifyEmail();
  Future<Either<Exception, void>> logout();
}
