import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:helpers/logic/auth/auth_facade.dart';
import 'package:rx_shared_preferences/rx_shared_preferences.dart';

import '../introduction/introduction.dart';

typedef Introduction = bool;

typedef FullAuthState = Either<AuthState?, Introduction>;

class AuthStateCubit extends Cubit<FullAuthState> {
  final AuthFacade _auth;
  final SharedPreferences _sp;
  StreamSubscription? _subscription;
  AuthStateCubit(this._sp, this._auth)
      : super(finishedIntroduction(_sp) ? Left(null) : Right(true)) {
    // _sp.clear(); // TODO: tmp
    if (finishedIntroduction(_sp)) {
      _startSubscription();
    }
  }

  void introductionFinished() {
    finishIntroduction(_sp);
    _startSubscription();
  }

  void _startSubscription() {
    _subscription = _auth.getStateStream().listen(
          (state) => emit(Left(state)),
        );
  }

  @override
  Future<void> close() async {
    _subscription?.cancel();
    super.close();
  }
}
