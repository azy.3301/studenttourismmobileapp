import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';

import 'core.dart';

class SimpleCubit<V> extends Cubit<BlocState<V>> {
  final Future<Either<Exception, V>> Function() _load;
  SimpleCubit(this._load) : super(const None()) {
    refresh();
  }

  Future<void> refresh() async {
    await _load().then(
      (result) => isClosed
          ? null
          : result.fold(
              (exception) => emit(Some(Left(exception))),
              (success) => emit(Some(Right(success))),
            ),
    );
  }
}
