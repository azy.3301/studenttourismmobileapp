import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

typedef BlocState<S> = Option<Either<Exception, S>>;

class StateNotLoaded implements Exception {}

extension BlocStateExtension<S extends Equatable> on BlocState<S> {
  Option<Return> forLoaded<Return>(Return Function(S) f) => fold(
        () => None(),
        (some) => some.fold((l) => None(), (s) => Some(f(s))),
      );
  S assertLoaded() => fold(
        () => throw StateNotLoaded(),
        (some) => some.fold(
          (e) => throw StateNotLoaded(),
          (l) => l,
        ),
      );
}

typedef UseCaseRes<V> = Either<Exception, V>;
