typedef ExtensionEmitter<S> = Function(S);
typedef StateGetter<S> = S Function();
