import 'package:bloc/bloc.dart';

class SimpleStateCubit<S> extends Cubit<S> {
  SimpleStateCubit(S initial) : super(initial);

  void setState(S newState) => emit(newState);
}
