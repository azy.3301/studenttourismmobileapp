import 'package:shared_preferences/shared_preferences.dart';

const _key = "INTRODUCTION_FINISHED";

void finishIntroduction(SharedPreferences sp) {
  sp.setBool(_key, true);
}

bool finishedIntroduction(SharedPreferences sp) {
  return sp.getBool(_key) ?? false;
}
