import 'package:helpers/logic/http/http.dart';

import '../core.dart';
import 'network_use_case_factory.dart';

// UniqueNetworkCRUD is a NetworkCRUD for an object that does not require an id for requests
// E.g. a Profile object that is created for every user automatically
// Therefore, it cannot be CREATEd or DELETEd

typedef UniqueReader<V> = Future<UseCaseRes<V>> Function();
typedef UniqueUpdater<V> = Future<UseCaseRes<void>> Function(V);

class UniqueNetworkCRUD {
  final NetworkUseCaseFactory _networkFactory;
  const UniqueNetworkCRUD(this._networkFactory);

  UniqueReader<V> newReader<V>({
    required String endpoint,
    required OutMapper<V> outMap,
  }) {
    return () => _networkFactory.newBaseNetworkUseCase(
          inpMapper: NoInpMapper(),
          getUri: (_, host) => Uri.https(host, endpoint),
          method: HTTPMethods.get,
          outMapper: outMap,
        )(null);
  }

  UniqueUpdater<V> newUpdater<V>({
    required String endpoint,
    required InpMapper<V> inpMap,
  }) {
    return _networkFactory.newBaseNetworkUseCase<V, void>(
      inpMapper: inpMap,
      getUri: (_, String host) => Uri.https(host, endpoint),
      method: HTTPMethods.patch,
      outMapper: NoOutMapper(),
    );
  }
}
