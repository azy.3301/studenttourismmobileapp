import 'package:helpers/logic/entity/entity.dart';
import 'package:helpers/logic/entity/network_use_case_factory.dart';

class CreatedIdMapper extends OutMapper<CreatedId> {
  @override
  CreatedId fromJson(Map<String, dynamic> json) => CreatedId(id: json['id']);
}
