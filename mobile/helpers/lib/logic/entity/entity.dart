import 'package:equatable/equatable.dart';

typedef Value = Equatable;

class CreatedId extends Equatable {
  final String id;
  @override
  List get props => [id];
  const CreatedId({required this.id});
}

class Entity<V> extends Equatable {
  final String id;
  final V o;
  @override
  List get props => [o, id];
  const Entity(this.id, this.o);
}
