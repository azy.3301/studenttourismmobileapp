import 'package:helpers/logic/http/http.dart';

import '../core.dart';
import 'entity.dart';
import 'network_use_case_factory.dart';

typedef Creator<V> = Future<UseCaseRes<Entity<V>>> Function(V);
typedef Reader<V> = Future<UseCaseRes<Entity<V>>> Function(String id);
typedef Updater<V> = Future<UseCaseRes<void>> Function(Entity<V>);
typedef Deleter<V> = Future<UseCaseRes<void>> Function(String id);

class NetworkCRUD {
  final NetworkUseCaseFactory _networkFactory;
  const NetworkCRUD(this._networkFactory);

  Uri _getURLWithId(String endpoint, String host, String id) => Uri.https(host, "$endpoint/$id");

  Creator<V> newCreator<V>({
    required String endpoint,
    required InpMapper<V> inpMap,
    required OutMapper<Entity<V>> outMap,
  }) {
    return _networkFactory.newBaseNetworkUseCase(
      inpMapper: inpMap,
      getUri: (_, host) => Uri.https(host, endpoint),
      method: HTTPMethods.post,
      outMapper: outMap,
    );
  }

  Reader<V> newReader<V>({
    required String endpoint,
    required OutMapper<Entity<V>> outMap,
  }) {
    return _networkFactory.newBaseNetworkUseCase(
      inpMapper: NoInpMapper(),
      getUri: (id, host) => _getURLWithId(endpoint, host, id),
      method: HTTPMethods.get,
      outMapper: outMap,
    );
  }

  Updater<V> newUpdater<V>({
    required String endpoint,
    required InpMapper<Entity<V>> inpMap,
  }) {
    return _networkFactory.newBaseNetworkUseCase(
      inpMapper: inpMap,
      getUri: (Entity<V> e, String host) => _getURLWithId(endpoint, host, e.id),
      method: HTTPMethods.patch,
      outMapper: NoOutMapper(),
    );
  }

  Deleter<V> newDeleter<V>({required String endpoint}) {
    return _networkFactory.newBaseNetworkUseCase(
      inpMapper: NoInpMapper(),
      getUri: (id, host) => _getURLWithId(endpoint, host, id),
      method: HTTPMethods.delete,
      outMapper: NoOutMapper(),
    );
  }
}
