import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../core.dart';

enum UpdateState {
  unchanged,
  editing,
  updating,
  updated,
}

class FormCubitState<V> extends Equatable {
  final V? initial;
  final V? val;
  final UpdateState upd;
  final Exception? exception;
  @override
  List get props => [initial, val, upd, exception];

  bool get isEditing => upd != UpdateState.unchanged;

  const FormCubitState([this.initial, this.val, this.upd = UpdateState.unchanged, this.exception]);

  bool updateAvailable() => initial != val && val != null;
  FormCubitState<V> withValue(V? val) => FormCubitState<V>(initial, val, upd, exception);
  FormCubitState<V> withUpdState(UpdateState upd) =>
      FormCubitState<V>(initial, val, upd, exception);
  FormCubitState<V> withException(Exception? e) => FormCubitState<V>(initial, val, upd, e);
}

class FormCubit<V> extends Cubit<FormCubitState<V>> {
  final Future<UseCaseRes<V>> Function() _read;
  final Future<UseCaseRes<void>> Function(V) _upd;
  FormCubit(
    this._read,
    this._upd,
  ) : super(const FormCubitState()) {
    _fetchAndEmit();
  }

  void editPressed() {
    emit(state.withUpdState(UpdateState.editing));
  }

  void cancelPressed() {
    emit(state.withUpdState(UpdateState.unchanged).withValue(state.initial));
  }

  void valueEdited(V newValue) {
    emit(state.withValue(newValue));
  }

  void updatePressed() {
    final currentVal = state.val;
    if (currentVal != null) {
      _updateAndEmit(currentVal);
    }
  }

  void _fetchAndEmit() {
    _read().then(
      (res) => res.fold(
        (e) => emit(state.withException(e)),
        (newV) => emit(FormCubitState(newV, newV)),
      ),
    );
  }

  void _updateAndEmit(V newV) {
    emit(state.withUpdState(UpdateState.updating));
    _upd(newV).then(
      (res) => res.fold(
        (e) => emit(state.withUpdState(UpdateState.editing).withException(e)),
        (success) => emit(FormCubitState(newV, newV, UpdateState.updated)),
      ),
    );
  }
}
