import 'package:http/http.dart' as http;

import '../auth/auth_facade.dart';

class NoTokenException implements Exception {}

typedef AuthHTTPClientFactory = AuthHTTPClient Function(http.Client);

AuthHTTPClientFactory newAuthHTTPClientFactory(AuthFacade auth) => (client) => AuthHTTPClient(
      auth,
      client,
    );

/// Adds the Authorization header using [AuthFacade] (therefore can throw [NoTokenException])
class AuthHTTPClient extends http.BaseClient {
  final AuthFacade _authFacade;
  final http.Client _inner;
  AuthHTTPClient(this._authFacade, this._inner);

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    final token = await _getTokenOrThrow();
    request.headers['Authorization'] = token;
    print(token);
    return _inner.send(request);
  }

  Future<String> _getTokenOrThrow() => _authFacade.getState().then(
        (stateOption) => stateOption.fold(
          () => throw NoTokenException(),
          (state) => state.token,
        ),
      );
}
