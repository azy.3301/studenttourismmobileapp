import 'package:http/http.dart' as http;

class JsonHTTPClient extends http.BaseClient {
  final http.Client _inner;
  JsonHTTPClient(this._inner);

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers["contentType"] = "application/json";
    request.headers["responseType"] = "application/json";
    return _inner.send(request);
  }
}
