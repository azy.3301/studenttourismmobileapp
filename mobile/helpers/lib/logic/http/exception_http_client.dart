import 'package:helpers/logic/general.dart';
import 'package:http/http.dart' as http;

import '../errors/errors.dart';

class ExceptionHTTPClient extends http.BaseClient {
  final http.Client _inner;
  ExceptionHTTPClient(this._inner);

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    printDebug(request);
    final streamedResp = await _inner.send(request);
    if (streamedResp.statusCode != 200) {
      final response = await http.Response.fromStream(streamedResp);
      throw NetworkException(response.statusCode, response.body);
    }
    return streamedResp;
  }
}
