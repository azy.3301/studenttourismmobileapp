class HTTPMethods {
  static const get = "GET";
  static const patch = "PATCH";
  static const put = "PUT";
  static const post = "POST";
  static const delete = "DELETE";
}
