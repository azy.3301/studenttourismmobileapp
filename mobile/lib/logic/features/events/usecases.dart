import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:studtourism/logic/core/models/student_event.dart';

typedef GetStudentEventsQueryUseCase = Query<StudentEvent> Function();

GetStudentEventsQueryUseCase newGetStudentEventsQueryUseCase(FullMapper<StudentEvent> map) =>
    () => FirebaseFirestore.instance.collection('events').withConverter<StudentEvent>(
          fromFirestore: (snapshot, _) => map.fromJson(snapshot.data()!),
          toFirestore: (dorm, _) => map.toJson(dorm),
        );
