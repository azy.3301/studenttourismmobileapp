import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:studtourism/logic/core/models/student_event.dart';

import '../../../core/models/university.dart';

class StudentEventMapper implements FullMapper<StudentEvent> {
  final FullMapper<University> uniMapper;
  const StudentEventMapper(this.uniMapper);
  @override
  StudentEvent fromJson(Map<String, dynamic> json) => StudentEvent(
        id: json['id'],
        university: uniMapper.fromJson(json['university']),
        description: json["description"],
        name: json["name"],
        price: (json["price"] as num).toDouble(),
        photoURL: json["photoURL"],
      );

  @override
  Map<String, dynamic> toJson(StudentEvent v) => {
        'id': v.id,
        'university': uniMapper.toJson(v.university),
        'description': v.description,
        'photoURL': v.photoURL,
        'name': v.name,
        'price': v.price,
      };
}
