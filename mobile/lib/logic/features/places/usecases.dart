import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:studtourism/logic/core/models/lab_place.dart';

typedef GetLabPlacesQueryUseCase = Query<LabPlace> Function();

GetLabPlacesQueryUseCase newGetLabPlacesQueryUseCase(FullMapper<LabPlace> map) =>
    () => FirebaseFirestore.instance.collection('places').withConverter<LabPlace>(
          fromFirestore: (snapshot, _) => map.fromJson(snapshot.data()!),
          toFirestore: (dorm, _) => map.toJson(dorm),
        );
