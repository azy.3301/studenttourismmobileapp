import 'package:helpers/logic/entity/network_use_case_factory.dart';

import '../../../core/models/lab_place.dart';

class LabPlaceMapper implements FullMapper<LabPlace> {
  @override
  LabPlace fromJson(Map<String, dynamic> json) => LabPlace(
        id: json["id"],
        name: json["name"],
        websiteURL: json["websiteURL"],
        photoURL: json["photoURL"],
        city: json["city"],
        description: json["description"],
      );

  @override
  Map<String, dynamic> toJson(LabPlace v) => {
        'id': v.id,
        'name': v.name,
        'websiteURL': v.websiteURL,
        'photoURL': v.photoURL,
        'city': v.city,
        'description': v.description,
      };
}
