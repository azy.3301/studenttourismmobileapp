import 'package:helpers/logic/auth/auth_facade.dart';
import 'package:helpers/logic/core.dart';
import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:helpers/logic/errors/errors.dart';
import 'package:studtourism/logic/core/db/database.dart';
import 'package:studtourism/logic/features/profile/values.dart';

typedef GetProfileUseCase = Future<UseCaseRes<ProfileInfo>> Function();
typedef UpdateProfileUseCase = Future<UseCaseRes<void>> Function(ProfileInfo);

const _profilesCollection = "profiles";

GetProfileUseCase newGetProfileUseCase(AuthFacade auth, Database db, OutMapper<ProfileInfo> map) =>
    () => withExceptionHandling(() async {
          final user = await auth.forceGetState();
          print(user.token);
          final profileJson = await db.get([_profilesCollection, user.id], def: {});
          return map.fromJson(profileJson);
        });

UpdateProfileUseCase newUpdateProfileUseCase(
        AuthFacade auth, Database db, InpMapper<ProfileInfo> map) =>
    (ProfileInfo upd) => withExceptionHandling(() async {
          final user = await auth.forceGetState();
          await db.update([_profilesCollection, user.id], map.toJson(upd));
          if (upd.name != null) await auth.updateDisplayName(upd.name!);
        });
