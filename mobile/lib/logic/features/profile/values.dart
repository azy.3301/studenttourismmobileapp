import 'package:equatable/equatable.dart';

class ProfileInfo extends Equatable {
  final String? name;
  final String? lastName;
  final String? patronymic;
  final String? vkLink;
  final String? city;
  final String? phone;
  @override
  List get props => [name, lastName, patronymic, vkLink, city, phone];
  const ProfileInfo({
    required this.name,
    required this.lastName,
    required this.patronymic,
    required this.vkLink,
    required this.city,
    required this.phone,
  });

  ProfileInfo copyWith({
    String? name,
    String? lastName,
    String? patronymic,
    String? vkLink,
    String? city,
    String? phone,
  }) =>
      ProfileInfo(
        name: name ?? this.name,
        lastName: lastName ?? this.lastName,
        patronymic: patronymic ?? this.patronymic,
        vkLink: vkLink ?? this.vkLink,
        city: city ?? this.city,
        phone: phone ?? this.phone,
      );
}
