import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:studtourism/logic/features/profile/values.dart';

class ProfileInfoMapper implements FullMapper<ProfileInfo> {
  @override
  ProfileInfo fromJson(Map<String, dynamic> json) {
    return ProfileInfo(
      name: json["name"],
      lastName: json["last_name"],
      patronymic: json["patronymic"],
      vkLink: json["vkLink"],
      city: json["city"],
      phone: json["phone"],
    );
  }

  @override
  Map<String, dynamic> toJson(ProfileInfo v) => {
        "name": v.name,
        "last_name": v.lastName,
        "patronymic": v.patronymic,
        "vkLink": v.vkLink,
        "city": v.city,
        "phone": v.phone,
      };
}
