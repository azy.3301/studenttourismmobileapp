import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:studtourism/logic/core/models/university.dart';

class UniMapper implements FullMapper<University> {
  @override
  University fromJson(Map<String, dynamic> json) => University(
        id: json["id"],
        city: json["city"],
        name: json["name"],
      );

  @override
  Map<String, dynamic> toJson(University v) => {
        "id": v.id,
        "city": v.city,
        "name": v.name,
      };
}
