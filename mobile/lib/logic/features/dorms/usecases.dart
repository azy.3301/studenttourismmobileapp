import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:helpers/logic/entity/network_use_case_factory.dart';

import '../../core/models/dorm.dart';

typedef GetDormsQueryUseCase = Query<Dorm> Function();

GetDormsQueryUseCase newGetDormsQueryUseCase(FullMapper<Dorm> map) =>
    () => FirebaseFirestore.instance.collection('dorms').withConverter<Dorm>(
          fromFirestore: (snapshot, _) => map.fromJson(snapshot.data()!),
          toFirestore: (dorm, _) => map.toJson(dorm),
        );
