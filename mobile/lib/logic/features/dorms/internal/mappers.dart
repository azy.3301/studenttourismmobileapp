import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:studtourism/logic/core/models/dorm.dart';

import '../../../core/models/university.dart';

class DormMapper implements FullMapper<Dorm> {
  final FullMapper<University> uniMap;
  const DormMapper(this.uniMap);
  @override
  Dorm fromJson(Map<String, dynamic> json) => Dorm(
        id: json["id"],
        uni: uniMap.fromJson(json["university"]),
        details: DormDetails(
          photoURL: json["details"]["photoURL"],
          name: json["details"]["name"],
          rooms: (json["details"]["rooms"] as List)
              .map(
                (roomJson) => _RoomMapper().fromJson(roomJson),
              )
              .toList(),
        ),
      );

  @override
  Map<String, dynamic> toJson(Dorm v) => {
        "id": v.id,
        "university": uniMap.toJson(v.uni),
        "details": {
          "photoURL": v.details.photoURL,
          "name": v.details.name,
          "rooms": v.details.rooms.map((room) => _RoomMapper().toJson(room)).toList(),
        },
      };
}

class _RoomMapper implements FullMapper<Room> {
  @override
  Room fromJson(Map<String, dynamic> json) => Room(
        type: json["type"],
        peopleCount: json["peopleCount"],
        price: (json["price"] as num).toDouble(),
      );

  @override
  Map<String, dynamic> toJson(Room v) => {
        'type': v.type,
        'peopleCount': v.peopleCount,
        'price': v.price,
      };
}
