import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:helpers/logic/auth/auth_facade.dart';
import 'package:helpers/logic/core.dart';
import 'package:helpers/logic/errors/errors.dart';
import 'package:studtourism/logic/features/booking%20/values.dart';
import 'package:uuid/uuid.dart';

import '../profile/usecases.dart';
import 'internal/mappers.dart';

typedef CreateBookingUseCase = Future<UseCaseRes<void>> Function(NewBooking);
typedef GetBookingsQueryUseCase = Future<UseCaseRes<Query<Booking>>> Function();

CreateBookingUseCase newCreateBookingUseCase(
  AuthFacade auth,
  GetProfileUseCase getProfile,
  BookingMapper map,
) =>
    (nb) => withExceptionHandling(() async {
          final profile = (await getProfile()).fold((e) => throw e, (p) => p);
          final booking = Booking(
            id: Uuid().v1(),
            target: nb.target,
            targetType: nb.targetType,
            dates: nb.dates,
            userId: (await auth.forceGetState()).id,
            profile: profile,
            status: BookingStatus.pending,
          );
          final ref = FirebaseFirestore.instance.collection("bookings").doc(booking.id);
          final doc = map.toJson(booking);
          await ref.set(doc);
        });

GetBookingsQueryUseCase newGetBookingsQueryUseCase(AuthFacade auth, BookingMapper map) =>
    () => withExceptionHandling(
          () async {
            final userId = (await auth.forceGetState()).id;
            return FirebaseFirestore.instance
                .collection("bookings")
                .where("userId", isEqualTo: userId)
                .withConverter<Booking>(
                  fromFirestore: (snapshot, _) => map.fromJson(snapshot.data()!),
                  toFirestore: (booking, _) => map.toJson(booking),
                );
          },
        );
