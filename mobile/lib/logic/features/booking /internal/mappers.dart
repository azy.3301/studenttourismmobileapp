import 'package:helpers/logic/entity/network_use_case_factory.dart';
import 'package:helpers/logic/general.dart';
import 'package:studtourism/logic/features/booking%20/values.dart';
import 'package:studtourism/logic/features/dorms/internal/mappers.dart';
import 'package:studtourism/logic/features/places/internal/mappers.dart';
import 'package:studtourism/logic/features/profile/internal/mappers.dart';

import '../../events/internal/mappers.dart';

class BookingMapper implements FullMapper<Booking> {
  final ProfileInfoMapper profileMap;
  final LabPlaceMapper placeMap;
  final DormMapper dormMap;
  final StudentEventMapper eventMap;
  const BookingMapper(this.profileMap, this.placeMap, this.dormMap, this.eventMap);
  @override
  Booking fromJson(Map<String, dynamic> json) {
    print(json);
    final type = _decodeTargetType(json["targetType"]);
    return Booking(
      id: json["id"],
      target: _decodeTarget(json["target"], type),
      targetType: type,
      userId: json["userId"],
      profile: profileMap.fromJson(json["profile"]),
      status: _decodeBookingStatus(json["status"]),
      dates: json["dates"] != null ? _DatesMapper().fromJson(json["dates"]) : null,
    );
  }

  @override
  Map<String, dynamic> toJson(Booking v) => {
        'id': v.id,
        'target': _encodeTarget(v.target, v.targetType),
        'targetType': _encodeTargetType(v.targetType),
        'userId': v.userId,
        'dates': v.dates != null ? _DatesMapper().toJson(v.dates!) : null,
        'profile': profileMap.toJson(v.profile),
        'status': _encodeBookingStatus(v.status),
      };

  Map<String, dynamic> _encodeTarget(dynamic target, TargetType type) {
    switch (type) {
      case TargetType.place:
        return placeMap.toJson(target);
      case TargetType.event:
        return eventMap.toJson(target);
      case TargetType.dorm:
        return dormMap.toJson(target);
    }
  }

  dynamic _decodeTarget(Map<String, dynamic> target, TargetType type) {
    switch (type) {
      case TargetType.place:
        return placeMap.fromJson(target);
      case TargetType.event:
        return eventMap.fromJson(target);
      case TargetType.dorm:
        return dormMap.fromJson(target);
    }
  }
}

class _DatesMapper implements FullMapper<Dates> {
  @override
  Dates fromJson(Map<String, dynamic> json) => Dates(
        fromUnixSec(json["begin"]),
        fromUnixSec(json["end"]),
      );

  @override
  Map<String, dynamic> toJson(Dates v) => {};
}

String _encodeTargetType(TargetType type) {
  switch (type) {
    case TargetType.place:
      return "place";
    case TargetType.event:
      return "event";
    case TargetType.dorm:
      return "dorm";
  }
}

TargetType _decodeTargetType(String type) {
  switch (type) {
    case "place":
      return TargetType.place;
    case "event":
      return TargetType.event;
    case "dorm":
      return TargetType.dorm;
    default:
      throw Exception("unknown target type $type");
  }
}

String _encodeBookingStatus(BookingStatus status) {
  switch (status) {
    case BookingStatus.pending:
      return "pending";
    case BookingStatus.accepted:
      return "accepted";
    case BookingStatus.rejected:
      return "rejected";
  }
}

BookingStatus _decodeBookingStatus(String status) {
  switch (status) {
    case "pending":
      return BookingStatus.pending;
    case "rejected":
      return BookingStatus.rejected;
    case "accepted":
      return BookingStatus.accepted;
    default:
      throw Exception("unknown booking status $status");
  }
}
