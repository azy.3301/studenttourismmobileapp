import 'package:equatable/equatable.dart';
import 'package:studtourism/logic/features/profile/values.dart';

enum TargetType {
  place,
  event,
  dorm,
}

enum BookingStatus {
  pending,
  accepted,
  rejected,
}

class Dates extends Equatable {
  final DateTime begin;
  final DateTime end;
  @override
  List get props => [begin, end];
  const Dates(this.begin, this.end);
}

class Booking extends Equatable {
  final String id;
  final dynamic target;
  final TargetType targetType;
  final String userId;
  final ProfileInfo profile;
  final Dates? dates;
  final BookingStatus status;
  @override
  List get props => [id, target, targetType, userId, profile, dates, status];

  const Booking({
    required this.id,
    required this.target,
    required this.targetType,
    required this.userId,
    required this.profile,
    required this.dates,
    required this.status,
  });
}

class NewBooking extends Equatable {
  final dynamic target;
  final TargetType targetType;
  final Dates? dates;
  @override
  List get props => [target, targetType, dates];
  const NewBooking({
    required this.target,
    required this.targetType,
    required this.dates,
  });
}
