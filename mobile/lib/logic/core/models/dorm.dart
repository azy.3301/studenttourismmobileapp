import 'package:equatable/equatable.dart';
import 'package:studtourism/logic/core/models/university.dart';

class Dorm extends Equatable {
  final String id;
  final University uni;
  final DormDetails details;

  @override
  List get props => [id, uni, details];

  const Dorm({
    required this.id,
    required this.uni,
    required this.details,
  });
}

class DormDetails extends Equatable {
  final String photoURL;
  final String? name;
  final List<Room> rooms;

  @override
  List get props => [name, rooms];

  const DormDetails({required this.photoURL, required this.name, required this.rooms});
}

class Room extends Equatable {
  final String? type;
  final double? peopleCount;
  final double? price;

  @override
  List get props => [type, peopleCount, price];

  const Room({this.type, this.peopleCount, this.price});
}
