import 'package:equatable/equatable.dart';

class LabPlace extends Equatable {
  final String id;
  final String name;
  final String websiteURL;
  final String photoURL;
  final String city;
  final String description;
  @override
  List get props => [id, name, websiteURL, photoURL, city, description];

  const LabPlace({
    required this.id,
    required this.name,
    required this.websiteURL,
    required this.photoURL,
    required this.city,
    required this.description,
  });
}
