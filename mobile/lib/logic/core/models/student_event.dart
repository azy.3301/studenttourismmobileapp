import 'package:equatable/equatable.dart';
import 'package:studtourism/logic/core/models/university.dart';

class StudentEvent extends Equatable {
  final String id;
  final University university;
  final String description;
  final String name;
  final String photoURL;
  final double price;

  @override
  List get props => [id, photoURL, university, description, name, price];

  const StudentEvent({
    required this.id,
    required this.photoURL,
    required this.university,
    required this.description,
    required this.name,
    required this.price,
  });
}
