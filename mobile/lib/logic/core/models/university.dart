import 'package:equatable/equatable.dart';

class University extends Equatable {
  final String id;
  final String city;
  final String name;

  @override
  List get props => [id, city, name];

  const University({
    required this.id,
    required this.city,
    required this.name,
  });
}
