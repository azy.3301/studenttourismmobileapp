typedef DBPath = List<String>;
typedef Document = Map<String, dynamic>;

abstract class Database {
  Future<Document> get(DBPath path, {Document? def});
  Future<void> update(DBPath path, Document values);
}
