import 'package:equatable/equatable.dart';

class NotFoundException implements Exception {}

class DatabaseException extends Equatable implements Exception {
  final dynamic lowLevel;
  @override
  List get props => [lowLevel];
  const DatabaseException(this.lowLevel);
}
