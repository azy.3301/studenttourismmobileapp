import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:studtourism/logic/core/db/database.dart';
import 'package:studtourism/logic/core/db/exceptions.dart';

class FSDatabase implements Database {
  final FirebaseFirestore _db;
  const FSDatabase(this._db);

  @override
  Future<Document> get(DBPath path, {Document? def}) async {
    try {
      final snapshot = await _db.doc(_convertPath(path)).get();
      final data = snapshot.data();
      if (data == null) {
        if (def != null) return def;
        throw NotFoundException();
      }
      return data;
    } catch (e) {
      if (e is NotFoundException) rethrow;
      throw DatabaseException(e);
    }
  }

  @override
  Future<void> update(DBPath path, Document values) async {
    try {
      final document = _db.doc(_convertPath(path));
      await document.set(values, SetOptions(merge: true));
    } catch (e) {
      throw DatabaseException(e);
    }
  }

  String _convertPath(DBPath path) => path.join("/");
}
