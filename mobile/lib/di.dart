import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:helpers/logic/auth/auth_facade.dart';
import 'package:helpers/logic/forms/form_cubit.dart';
import 'package:helpers/ui/errors/bloc_exception_listener.dart';
import 'package:helpers/ui/forms/form_widget.dart';
import 'package:helpers/ui/general/simple_cubit_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:studtourism/logic/core/db/firestore/firestore_database.dart';
import 'package:studtourism/logic/features/booking%20/internal/mappers.dart';
import 'package:studtourism/logic/features/dorms/internal/mappers.dart';
import 'package:studtourism/logic/features/events/internal/mappers.dart';
import 'package:studtourism/logic/features/places/internal/mappers.dart';
import 'package:studtourism/logic/features/profile/internal/mappers.dart';
import 'package:studtourism/logic/features/profile/usecases.dart';
import 'package:studtourism/logic/features/universities/internal/mappers.dart';

import 'logic/features/auth/auth_facade.dart';
import 'logic/features/booking /usecases.dart';
import 'logic/features/dorms/usecases.dart';
import 'logic/features/events/usecases.dart';
import 'logic/features/places/usecases.dart';
import 'logic/features/profile/values.dart';

class UIDeps {
  final AuthFacade authFacade;
  final SharedPreferences sp;
  final SimpleBuilderFactory simpleBuilder;
  final FormWidgetFactory formWidget;
  final BlocExceptionListenerFactory exceptionListener;

  final FormCubit<ProfileInfo> Function() profileFormFactory;
  final GetDormsQueryUseCase getDormsQuery;
  final GetStudentEventsQueryUseCase getEventsQuery;
  final GetLabPlacesQueryUseCase getPlacesQuery;

  final CreateBookingUseCase createBooking;
  final GetBookingsQueryUseCase getBookingsQuery;

  UIDeps._(
    this.authFacade,
    this.sp,
    this.simpleBuilder,
    this.formWidget,
    this.exceptionListener,
    this.profileFormFactory,
    this.getDormsQuery,
    this.getEventsQuery,
    this.getPlacesQuery,
    this.createBooking,
    this.getBookingsQuery,
  );
}

late final UIDeps uiDeps;

Future<void> initialize() async {
  final auth = FirebaseAuthFacade(FirebaseAuth.instance);
  final db = FSDatabase(FirebaseFirestore.instance);

  final exceptionListener = newBlocExceptionListenerFactory(auth);
  final formWidget = newFormWidgetFactory(exceptionListener);
  final simpleBuilder = newSimpleBuilderFactory(exceptionListener);

  final profileMapper = ProfileInfoMapper();
  final getProfile = newGetProfileUseCase(auth, db, profileMapper);
  final updProfile = newUpdateProfileUseCase(auth, db, profileMapper);

  final uniMapper = UniMapper();
  final dormMapper = DormMapper(uniMapper);
  final eventMapper = StudentEventMapper(uniMapper);
  final placeMapper = LabPlaceMapper();
  final bookingMapper = BookingMapper(profileMapper, placeMapper, dormMapper, eventMapper);

  final getDormsQuery = newGetDormsQueryUseCase(dormMapper);
  final getEventsQuery = newGetStudentEventsQueryUseCase(eventMapper);
  final getPlacesQuery = newGetLabPlacesQueryUseCase(placeMapper);

  final createBooking = newCreateBookingUseCase(auth, getProfile, bookingMapper);
  final getBookings = newGetBookingsQueryUseCase(auth, bookingMapper);

  uiDeps = UIDeps._(
    auth,
    await SharedPreferences.getInstance(),
    simpleBuilder,
    formWidget,
    exceptionListener,
    () => FormCubit(getProfile, updProfile),
    getDormsQuery,
    getEventsQuery,
    getPlacesQuery,
    createBooking,
    getBookings,
  );
}
