import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/app/auth_providers.dart';
import 'package:studtourism/ui/core/app/routing.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AuthProviders(
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routerConfig: router,
        darkTheme: createTheme(Brightness.dark),
        theme: createTheme(Brightness.light),
      ),
    );
  }
}
