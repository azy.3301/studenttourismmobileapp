import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:studtourism/ui/core/app/splash_screen.dart';
import 'package:studtourism/ui/features/auth/auth_screen.dart';
import 'package:studtourism/ui/features/dashboard/dashboard_screen.dart';
import 'package:studtourism/ui/features/dorms/dorms_screen.dart';
import 'package:studtourism/ui/features/events/events_screen.dart';
import 'package:studtourism/ui/features/introduction/my_introduction_screen.dart';
import 'package:studtourism/ui/features/places/places_screen.dart';

import '../../features/profile/profile_screen.dart';
import '../../features/settings/settings_screen.dart';
import 'auth_state_inherited_widget.dart';

enum Routes {
  login,
  dorms,
  places,
  dashboard,
  events,
  account,
  settings,
  introduction,
}

extension RoutePaths on Routes {
  String get path {
    switch (this) {
      case Routes.login:
        return "login";
      case Routes.dorms:
        return "dorms";
      case Routes.places:
        return "places";
      case Routes.events:
        return "events";
      case Routes.dashboard:
        return "dashboard";
      case Routes.account:
        return "account";
      case Routes.settings:
        return "settings";
      case Routes.introduction:
        return "introduction";
    }
  }

  String get fullPath => "/$path";
}

final router = GoRouter(
  redirect: (context, state) {
    // creates a subscription on AuthStateInheritedWidget
    // which causes this method to run every time a new auth state is emitted
    final authState = context.dependOnInheritedWidgetOfExactType<AuthStateInheritedWidget>()!.state;
    switch (authState) {
      case AuthenticationState.introduction:
        return Routes.introduction.fullPath;
      case AuthenticationState.loading:
        return "/";
      case AuthenticationState.unauthenticated:
        return Routes.login.fullPath;
      case AuthenticationState.authenticated:
        final isLoggingIn = [Routes.login.fullPath, "/"].contains(state.location);
        // if the user is not logging in, then do not redirect anywhere
        return isLoggingIn ? Routes.dashboard.fullPath : null;
    }
  },
  routes: <GoRoute>[
    GoRoute(
      path: "/",
      builder: (context, GoRouterState state) => SplashScreen(),
    ),
    GoRoute(
      path: Routes.login.fullPath,
      builder: (BuildContext context, GoRouterState state) => const AuthScreen(),
    ),
    GoRoute(
      path: Routes.dorms.fullPath,
      builder: (BuildContext context, GoRouterState state) => DormsScreen(),
    ),
    GoRoute(
      path: Routes.dashboard.fullPath,
      builder: (BuildContext context, GoRouterState state) => DashboardScreen(),
    ),
    GoRoute(
      path: Routes.places.fullPath,
      builder: (BuildContext context, GoRouterState state) => PlacesScreen(),
    ),
    GoRoute(
      path: Routes.introduction.fullPath,
      builder: (BuildContext context, GoRouterState state) => MyIntroductionScreen(),
    ),
    GoRoute(
      path: Routes.events.fullPath,
      builder: (BuildContext context, GoRouterState state) => EventsScreen(),
    ),
    GoRoute(
      path: Routes.account.fullPath,
      builder: (BuildContext context, GoRouterState state) => const NewProfileScreen(),
    ),
    GoRoute(
      path: Routes.settings.fullPath,
      builder: (BuildContext context, GoRouterState state) => SettingsScreen(),
    ),
  ],
);
