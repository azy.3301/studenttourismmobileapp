import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/general/helpers.dart';
import 'package:studtourism/ui/core/widgets/share_button.dart';

import '../general/themes/theme.dart';

class ActivityCard extends StatelessWidget {
  final String imageURL;
  final String title;
  final Widget contents;
  final Widget? footer;
  final String shareText;

  const ActivityCard({
    Key? key,
    required this.imageURL,
    required this.title,
    required this.contents,
    this.footer,
    required this.shareText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final text = theme.textTheme;
    return SizedBox(
      height: 275,
      child: Card(
        elevation: 5,
        shadowColor: AppColors.purple,
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 100,
                height: double.infinity,
                child: Image.network(
                  imageURL,
                  fit: BoxFit.fitHeight,
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      title,
                      style: text.headline4,
                      textAlign: TextAlign.left,
                    ),
                    contents,
                    Spacer(),
                    Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                      if (footer != null) footer!,
                      Spacer(),
                      ShareButton(text: shareText)
                    ])
                  ].withSpaceBetween(height: 5),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
