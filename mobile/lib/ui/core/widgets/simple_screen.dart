import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:studtourism/ui/core/app/routing.dart';

import '../general/themes/theme.dart';
import 'navigation_bar.dart';

class SimpleScreen extends StatelessWidget {
  final FloatingActionButton? floatingActionButton;
  final String title;
  final Widget Function(BuildContext) contentBuilder;
  final Future<void> Function()? onRefresh;
  const SimpleScreen({
    Key? key,
    required this.title,
    this.onRefresh,
    this.floatingActionButton,
    required this.contentBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final text = theme.textTheme;
    final mainContent = ListView(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Text(title, style: text.headline1),
        ),
        SizedBox(height: ThemeConstants.sectionSpacing),
        contentBuilder(context),
        SizedBox(height: ThemeConstants.sectionSpacing),
      ],
    );
    return WillPopScope(
      onWillPop: () async =>
          GoRouter.of(context).location !=
          Routes.dorms.fullPath, // TODO: remove this dirty workaround
      child: Scaffold(
        bottomNavigationBar: customNavigationBar(context),
        floatingActionButton: floatingActionButton,
        body: Padding(
          padding: ThemeConstants.screenPadding,
          child: onRefresh != null
              ? RefreshIndicator(onRefresh: onRefresh!, child: mainContent)
              : mainContent,
        ),
      ),
    );
  }
}
