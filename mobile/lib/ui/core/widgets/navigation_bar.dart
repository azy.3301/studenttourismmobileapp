import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../app/routing.dart';

BottomNavigationBar customNavigationBar(BuildContext context) => BottomNavigationBar(
      iconSize: 28,
      type: BottomNavigationBarType.fixed,
      currentIndex: getNavBarIndex(GoRouter.of(context).location),
      items: [
        BottomNavigationBarItem(
          icon: _RouteIcon(icon: Icons.house, route: Routes.dorms),
          label: "Жильё",
        ),
        BottomNavigationBarItem(
          icon: _RouteIcon(icon: Icons.travel_explore, route: Routes.places),
          label: "Места",
        ),
        BottomNavigationBarItem(
          icon: _RouteIcon(icon: Icons.dashboard, route: Routes.dashboard),
          label: "Меню",
        ),
        BottomNavigationBarItem(
          icon: _RouteIcon(icon: Icons.event, route: Routes.events),
          label: "События",
        ),
        BottomNavigationBarItem(
          icon: _RouteIcon(icon: Icons.person, route: Routes.account),
          label: "Профиль",
        ),
      ],
    );

int getNavBarIndex(String fullPath) {
  if (fullPath == Routes.dorms.fullPath)
    return 0;
  else if (fullPath == Routes.places.fullPath)
    return 1;
  else if (fullPath == Routes.dashboard.fullPath)
    return 2;
  else if (fullPath == Routes.events.fullPath)
    return 3;
  else if (fullPath == Routes.account.fullPath)
    return 4;
  else
    return 4;
}

class _RouteIcon extends StatelessWidget {
  final Routes route;
  final IconData icon;
  const _RouteIcon({Key? key, required this.route, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isSelected = GoRouter.of(context).location == route.fullPath;
    return IconButton(
      onPressed: () => context.go(route.fullPath),
      icon: Icon(icon, color: isSelected ? Theme.of(context).colorScheme.primary : null),
    );
  }
}
