import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/ui/features/dashboard/carousel_card.dart';

import '../../../di.dart';
import '../../../logic/core/models/dorm.dart';

class DormsCarousel extends StatelessWidget {
  const DormsCarousel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FirestoreListView<Dorm>(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      query: uiDeps.getDormsQuery(),
      itemBuilder: (BuildContext context, QueryDocumentSnapshot<Dorm> doc) => CarouselCard(
        color: CarouselCardColor.second,
        title: doc.data().uni.name,
        description: doc.data().details.name ?? "",
        photoURL: doc.data().details.photoURL,
      ),
    );
  }
}
