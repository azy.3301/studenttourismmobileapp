import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';

enum CarouselCardColor {
  first,
  second,
  third,
}

class CarouselCard extends StatelessWidget {
  final CarouselCardColor color;
  final String title;
  final String description;
  final String photoURL;

  const CarouselCard({
    Key? key,
    required this.color,
    required this.title,
    required this.description,
    required this.photoURL,
  }) : super(key: key);

  Color _getColor(CarouselCardColor c) {
    switch (c) {
      case CarouselCardColor.first:
        return AppColors.yellow;
      case CarouselCardColor.second:
        return AppColors.lightGreen;
      case CarouselCardColor.third:
        return AppColors.lightBlue;
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final text = theme.textTheme;
    return SizedBox(
      width: 200,
      child: Card(
        elevation: 5,
        color: _getColor(color),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 8),
              Center(
                child: SizedBox(
                  height: 100,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(photoURL, fit: BoxFit.scaleDown),
                  ),
                ),
              ),
              Text(
                title,
                style: text.headline1,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
              ),
              Text(description, overflow: TextOverflow.ellipsis),
              Spacer(),
              Align(
                alignment: Alignment.center,
                child: Text("Хочу попасть!"),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
