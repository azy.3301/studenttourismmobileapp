import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/ui/features/dashboard/carousel_card.dart';

import '../../../di.dart';
import '../../../logic/core/models/student_event.dart';

class EventsCarousel extends StatelessWidget {
  const EventsCarousel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FirestoreListView<StudentEvent>(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      query: uiDeps.getEventsQuery(),
      itemBuilder: (BuildContext context, QueryDocumentSnapshot<StudentEvent> doc) => CarouselCard(
        color: CarouselCardColor.first,
        title: doc.data().name,
        photoURL: doc.data().photoURL,
        description: "17 июня - 16 июля",
      ),
    );
  }
}
