import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/widgets/simple_screen.dart';

import 'dorms_carousel.dart';
import 'events_carousel.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    return SimpleScreen(
      title: "Начнём путешествие?",
      contentBuilder: (_) => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          CarouselSection(
            title: "События",
            carousel: EventsCarousel(),
          ),
          SizedBox(height: 20),
          CarouselSection(
            title: "Жильё",
            carousel: DormsCarousel(),
          ),
        ],
      ),
    );
  }
}

class CarouselSection extends StatelessWidget {
  final String title;
  final Widget carousel;
  const CarouselSection({
    Key? key,
    required this.title,
    required this.carousel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final text = theme.textTheme;
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(title, style: text.headline2, textAlign: TextAlign.left),
          SizedBox(height: 250, child: carousel),
        ]);
  }
}
