import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:helpers/logic/auth/auth_state_bloc.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';

class MyIntroductionScreen extends StatelessWidget {
  const MyIntroductionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme.headline2?.copyWith(color: AppColors.white);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: IntroductionScreen(
        globalBackgroundColor: AppColors.purple,
        done: Text("Начать", style: TextStyle(color: AppColors.white)),
        onDone: () => context.read<AuthStateCubit>().introductionFinished(),
        next: Text("Далее", style: TextStyle(color: AppColors.white)),
        pages: [
          PageViewModel(
            titleWidget: Container(),
            bodyWidget: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 100),
                Image.asset("assets/intro/1.png"),
                Text("Ты студент и мечтаешь путешествовать недорого?", style: textTheme),
              ],
            ),
          ),
          PageViewModel(
            titleWidget: Container(),
            bodyWidget: Column(
              children: [
                SizedBox(height: 100),
                Image.asset("assets/intro/2.png"),
                Text("Бронируй жильё и билеты на транспорт!", style: textTheme),
              ],
            ),
          ),
          PageViewModel(
            titleWidget: Container(),
            bodyWidget: Column(
              children: [
                SizedBox(height: 100),
                Image.asset("assets/intro/3.png"),
                Text("Ищи развлечения и актуальные мероприятия!", style: textTheme),
              ],
            ),
          ),
          PageViewModel(
            titleWidget: Container(),
            bodyWidget: Column(
              children: [
                SizedBox(height: 100),
                Image.asset("assets/intro/4.png"),
                Text("Мы поможем сделать это быстро и с наименьшими затратами!", style: textTheme),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
