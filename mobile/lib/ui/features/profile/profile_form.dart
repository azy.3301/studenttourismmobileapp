import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:helpers/logic/forms/form_cubit.dart';
import 'package:helpers/ui/forms/custom_form_text_field.dart';
import 'package:helpers/ui/forms/form_widget.dart';
import 'package:studtourism/di.dart';
import 'package:studtourism/logic/features/profile/values.dart';
import 'package:studtourism/ui/core/general/helpers.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';
import 'package:studtourism/ui/core/widgets/section_widget.dart';

class ProfileForm extends StatelessWidget {
  const ProfileForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return uiDeps.formWidget(
      uiDeps.profileFormFactory,
      (context, FormInfo<ProfileInfo> info) {
        final cubit = context.read<FormCubit<ProfileInfo>>();
        return SectionWidget(
          title: Text("Данные"),
          action: Padding(
            padding: const EdgeInsets.all(8.0),
            child: info.action,
          ),
          content: Column(
              children: [
            CustomFormTextField(
              label: "Имя",
              getValue: (ProfileInfo p) => p.name,
              updValue: (v) => cubit.valueEdited(info.current.copyWith(name: v)),
            ),
            CustomFormTextField(
              label: "Фамилия",
              getValue: (ProfileInfo p) => p.lastName,
              updValue: (v) => cubit.valueEdited(info.current.copyWith(lastName: v)),
            ),
            CustomFormTextField(
              label: "Отчество",
              getValue: (ProfileInfo p) => p.patronymic,
              updValue: (v) => cubit.valueEdited(info.current.copyWith(patronymic: v)),
            ),
            CustomFormTextField(
              label: "Ссылка на ВКонтакте",
              getValue: (ProfileInfo p) => p.vkLink,
              updValue: (v) => cubit.valueEdited(info.current.copyWith(vkLink: v)),
            ),
            CustomFormTextField(
              label: "Номер телефона",
              getValue: (ProfileInfo p) => p.phone,
              updValue: (v) => cubit.valueEdited(info.current.copyWith(phone: v)),
            ),
            CustomFormTextField(
              label: "Город",
              getValue: (ProfileInfo p) => p.city,
              updValue: (v) => cubit.valueEdited(info.current.copyWith(city: v)),
            ),
          ].withSpaceBetween(height: ThemeConstants.textFieldSpacing)),
        );
      },
    );
  }
}
