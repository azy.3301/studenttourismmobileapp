import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/general/helpers.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';
import 'package:studtourism/ui/core/widgets/simple_screen.dart';
import 'package:studtourism/ui/features/profile/profile_form.dart';

import '../../../di.dart';

class NewProfileScreen extends StatelessWidget {
  const NewProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleScreen(
      title: "Мой Профиль",
      contentBuilder: (_) => Column(
        children: [
          ProfileForm(),
          TextButton(
            onPressed: uiDeps.authFacade.logout,
            child: Text("Выйти", style: TextStyle(color: AppColors.red)),
          ),
        ].withSpaceBetween(height: ThemeConstants.sectionSpacing),
      ),
    );
  }
}
