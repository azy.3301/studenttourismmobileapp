import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/general/sharing.dart';
import 'package:studtourism/ui/core/widgets/activity_card.dart';

import '../../../logic/core/models/dorm.dart';

class DormCard extends StatelessWidget {
  final Dorm dorm;
  final Widget footer;
  const DormCard({Key? key, required this.dorm, required this.footer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final text = theme.textTheme;
    return ActivityCard(
      imageURL: dorm.details.photoURL,
      title: dorm.details.name ?? "Общежитие",
      contents: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Text(dorm.uni.name, style: text.headline5),
        Text(dorm.uni.city, style: text.headline5),
        SizedBox(height: 20),
        if (dorm.details.rooms.isNotEmpty)
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  Expanded(child: Text("Вариантов размещения", style: text.headline5)),
                  Expanded(child: Text("Стоимость на 1 человека", style: text.headline5)),
                ],
              ),
              SizedBox(height: 5),
              Row(
                children: [
                  Expanded(
                    child: Text("${dorm.details.rooms.length}", style: text.headline5),
                  ),
                  Expanded(child: Text("50", style: text.headline5)),
                ],
              ),
            ],
          ),
      ]),
      footer: footer,
      shareText: shareDorm(dorm),
    );
  }
}
