import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/ui/core/widgets/simple_screen.dart';
import 'package:studtourism/ui/features/booking/bookings_action_button.dart';

import '../../../di.dart';
import '../../../logic/core/models/dorm.dart';
import '../../../logic/features/booking /values.dart';
import '../booking/book_button.dart';
import 'dorm_card.dart';

class DormsScreen extends StatelessWidget {
  const DormsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleScreen(
      title: "Жильё",
      floatingActionButton: newBookingsActionButton(context),
      contentBuilder: (_) => Padding(
        padding: EdgeInsets.all(8.0),
        child: FirestoreListView<Dorm>(
          shrinkWrap: true,
          query: uiDeps.getDormsQuery(),
          itemBuilder: (BuildContext context, QueryDocumentSnapshot<Dorm> doc) => DormCard(
            dorm: doc.data(),
            footer: BookButton(
              book: NewBooking(
                target: doc.data(),
                targetType: TargetType.dorm,
                dates: null, // TODO: add dates
              ),
            ),
          ),
        ),
      ),
    );
  }
}
