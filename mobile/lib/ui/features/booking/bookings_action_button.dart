import 'package:flutter/material.dart';

import '../../core/general/themes/theme.dart';
import 'bookings.dart';

FloatingActionButton newBookingsActionButton(BuildContext context) => FloatingActionButton(
      child: Icon(Icons.list_alt),
      backgroundColor: AppColors.purple,
      onPressed: () =>
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => BookingsScreen())),
    );
