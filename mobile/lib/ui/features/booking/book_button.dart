import 'package:flutter/material.dart';

import '../../../di.dart';
import '../../../logic/features/booking /values.dart';
import 'handle_booking.dart';

class BookButton extends StatelessWidget {
  final NewBooking book;
  const BookButton({Key? key, required this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () =>
          handleBooking(context, uiDeps.createBooking(book)), // TODO add error handling
      child: Text("Оставить заявку"),
    );
  }
}
