import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/di.dart';
import 'package:studtourism/ui/features/booking/booking_card.dart';

import '../../../logic/features/booking /values.dart';
import '../../core/widgets/simple_screen.dart';

class BookingsScreen extends StatelessWidget {
  const BookingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleScreen(
      title: "Мои заявки",
      contentBuilder: (context) => Padding(
        padding: EdgeInsets.all(8),
        child: uiDeps.simpleBuilder<Query<Booking>>(
          load: uiDeps.getBookingsQuery,
          loadedBuilder: (context, query) => FirestoreListView<Booking>(
            emptyBuilder: (_) => Text("У вас пока нет заявок :("),
            shrinkWrap: true,
            query: query,
            itemBuilder: (BuildContext context, QueryDocumentSnapshot<Booking> doc) => BookingCard(
              booking: doc.data(),
            ),
          ),
        ),
      ),
    );
  }
}
