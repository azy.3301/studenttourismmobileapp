import 'package:flutter/material.dart';
import 'package:helpers/logic/core.dart';
import 'package:helpers/ui/errors/exception_snackbar.dart';

import '../../core/general/themes/theme.dart';

void handleBooking(BuildContext context, Future<UseCaseRes<void>> bookReq) {
  bookReq.then((res) => res.fold(
        (exception) => showExceptionSnackbar(context, exception),
        (success) => ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Row(children: [
              Text("Заявка успешно подана!", style: const TextStyle(color: Colors.white)),
              const Spacer(),
              TextButton(
                child: const Text("Dismiss", style: TextStyle(color: Colors.white)),
                onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
              ),
            ]),
            backgroundColor: AppColors.lightGreen,
          ),
        ),
      ));
}
