import 'package:flutter/material.dart';
import 'package:studtourism/logic/core/models/lab_place.dart';
import 'package:studtourism/logic/core/models/student_event.dart';
import 'package:studtourism/logic/features/booking%20/values.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';
import 'package:studtourism/ui/features/dorms/dorm_card.dart';
import 'package:studtourism/ui/features/events/event_card.dart';
import 'package:studtourism/ui/features/places/place_card.dart';

import '../../../logic/core/models/dorm.dart';

class BookingCard extends StatelessWidget {
  final Booking booking;
  const BookingCard({Key? key, required this.booking}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final footer = Text(
      "Рассматривается",
      style: TextStyle(color: AppColors.lightGreen), // TODO: handle other statuses
    );
    switch (booking.targetType) {
      case TargetType.place:
        return PlaceCard(
          place: booking.target as LabPlace,
          footer: footer,
        );
      case TargetType.event:
        return EventCard(
          event: booking.target as StudentEvent,
          footer: footer,
        );
      case TargetType.dorm:
        return DormCard(
          dorm: booking.target as Dorm,
          footer: footer,
        );
    }
  }
}
