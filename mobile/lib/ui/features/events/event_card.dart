import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/logic/core/models/student_event.dart';
import 'package:studtourism/ui/core/general/sharing.dart';
import 'package:studtourism/ui/core/widgets/activity_card.dart';

class EventCard extends StatelessWidget {
  final StudentEvent event;
  final Widget footer;
  const EventCard({
    Key? key,
    required this.event,
    required this.footer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ActivityCard(
      imageURL: event.photoURL,
      title: event.name,
      contents: Text(
        event.description,
        overflow: TextOverflow.ellipsis,
      ),
      footer: footer, // TODO add error handling
      shareText: shareEvent(event),
    );
  }
}
