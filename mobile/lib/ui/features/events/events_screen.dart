import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/logic/core/models/student_event.dart';
import 'package:studtourism/ui/core/widgets/simple_screen.dart';
import 'package:studtourism/ui/features/booking/bookings_action_button.dart';
import 'package:studtourism/ui/features/events/event_card.dart';

import '../../../di.dart';
import '../../../logic/features/booking /values.dart';
import '../booking/book_button.dart';

class EventsScreen extends StatelessWidget {
  const EventsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleScreen(
      title: "Мероприятия",
      floatingActionButton: newBookingsActionButton(context),
      contentBuilder: (_) => Padding(
        padding: EdgeInsets.all(8.0),
        child: FirestoreListView<StudentEvent>(
          shrinkWrap: true,
          primary: true,
          query: uiDeps.getEventsQuery(),
          itemBuilder: (BuildContext context, QueryDocumentSnapshot<StudentEvent> doc) => EventCard(
            event: doc.data(),
            footer: BookButton(
              book: NewBooking(
                target: doc.data(),
                targetType: TargetType.event,
                dates: null,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
