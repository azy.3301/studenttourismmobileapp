import 'package:firebase_ui_auth/firebase_ui_auth.dart';
import 'package:flutter/material.dart';

import '../../core/app/logo_widget.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SignInScreen(
        headerMaxExtent: 250,
        headerBuilder: (context, constraints, _) {
          return const Center(
            child: Padding(
              padding: EdgeInsets.only(top: 100, left: 20, right: 20),
              child: LogoWidget(),
            ),
          );
        },
        providers: [EmailAuthProvider()],
      ),
    );
  }
}
