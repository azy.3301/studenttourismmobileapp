import 'package:flutter/material.dart';
import 'package:studtourism/logic/core/models/lab_place.dart';
import 'package:studtourism/ui/core/general/sharing.dart';
import 'package:studtourism/ui/core/general/themes/theme.dart';
import 'package:studtourism/ui/core/widgets/activity_card.dart';
import 'package:url_launcher/url_launcher_string.dart';

class PlaceCard extends StatelessWidget {
  final LabPlace place;
  final Widget footer;
  const PlaceCard({Key? key, required this.place, required this.footer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ActivityCard(
      imageURL: place.photoURL,
      title: place.name,
      contents: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Text(place.city),
        SizedBox(height: 20),
        Text("Описание"),
        Text(
          place.description,
          overflow: TextOverflow.ellipsis,
        ),
        SizedBox(height: 30),
        LinkButton(url: place.websiteURL),
      ]),
      footer: footer, // TODO add error handling
      shareText: sharePlace(place),
    );
  }
}

class LinkButton extends StatelessWidget {
  final String url;
  const LinkButton({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(backgroundColor: AppColors.lightBlue),
      onPressed: () => launchUrlString(url),
      child: Text("Перейти на сайт"),
    );
  }
}
