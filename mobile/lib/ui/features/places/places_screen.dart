import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_ui_firestore/firebase_ui_firestore.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/logic/core/models/lab_place.dart';
import 'package:studtourism/logic/features/booking%20/values.dart';
import 'package:studtourism/ui/core/widgets/simple_screen.dart';
import 'package:studtourism/ui/features/booking/book_button.dart';
import 'package:studtourism/ui/features/booking/bookings_action_button.dart';
import 'package:studtourism/ui/features/places/place_card.dart';

import '../../../di.dart';

class PlacesScreen extends StatelessWidget {
  const PlacesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleScreen(
      title: "Куда сходить?",
      floatingActionButton: newBookingsActionButton(context),
      contentBuilder: (_) => Padding(
        padding: EdgeInsets.all(8.0),
        child: FirestoreListView<LabPlace>(
          shrinkWrap: true,
          query: uiDeps.getPlacesQuery(),
          itemBuilder: (BuildContext context, QueryDocumentSnapshot<LabPlace> doc) => PlaceCard(
            place: doc.data(),
            footer: BookButton(
              book: NewBooking(
                target: doc.data(),
                targetType: TargetType.place,
                dates: null,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
