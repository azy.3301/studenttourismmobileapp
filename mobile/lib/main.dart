import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:studtourism/firebase_options.dart';
import 'package:studtourism/ui/core/app/app.dart';

import 'di.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await di.initialize();
  runApp(MyApp());
}
