from flask import Flask, jsonify, request, send_file
from flask_sqlalchemy import SQLAlchemy
from redis import Redis
import json, os, logging, psycopg2

from psycopg2.extras import RealDictCursor

db = SQLAlchemy()
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:studstud@localhost:22223/postgres'
db.init_app(app)


# psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
#                  database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
#                  password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)

# print(os.getenv('POSTGRES_PASSWORD'))

# def get_pg_connection():
#     pg_conn = psycopg2.connect(host=os.getenv('POSTGRES_HOST') or '127.0.0.1', port=os.getenv('POSTGRES_PORT'),
#                                database=os.getenv('POSTGRES_DB'), user=os.getenv('POSTGRES_USER'),
#                                password=os.getenv('POSTGRES_PASSWORD'), cursor_factory=RealDictCursor)
#     pg_conn.autocommit = True
#     return pg_conn


# def get_redis_connection():
#     return Redis(host=os.getenv('REDIS_HOST') or '127.0.0.1', port=os.getenv('REDIS_PORT'),
#                  password=os.getenv('REDIS_PASSWORD'), decode_responses=True)


# # http://127.0.0.1:33334/app
# @app.route('/app')
# def matview():
#         return {}, 200

# @app.route('/')
# def hello_world():
#     return 'Moe Flask приложение в контейнере Docker.'
