from flask_app import db


class Achieve(db.Model):
    __tablename__ = "achievements"
    id = db.Column(db.Integer, primary_key=True)
    image_main = db.Column(db.String(80))
    title = db.Column(db.String(80))
    description = db.Column(db.String(1024))
    score = db.Column(db.Integer)
