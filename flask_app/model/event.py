from flask_app import db


class Event(db.Model):
    __tablename__ = "events"
    id = db.Column(db.Integer, primary_key=True)
    start_date = db.Column(db.DateTime)
    finish_date = db.Column(db.DateTime)
    title = db.Column(db.String(1024))
    description = db.Column(db.String(4096))
    image_main = db.Column(db.String(50))
    contact_name = db.Column(db.String(50))
    contact_phone = db.Column(db.String(20))
    contact_email = db.Column(db.String(50))
