from flask_app import db


class Room(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    university_id = db.Column(db.Integer)
    number_residents = db.Column(db.Integer)
    title = db.Column(db.String(80))
    description = db.Column(db.String(1024))
    address = db.Column(db.String(256))
    price = db.Column(db.Integer)
    food = db.Column(db.Boolean)
    min_time_days = db.Column(db.Integer)
    max_time_days = db.Column(db.Integer)
