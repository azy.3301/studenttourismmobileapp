from flask_app import db


class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80))
    phone = db.Column(db.String(20))
    vk = db.Column(db.String(80))
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    patronymic = db.Column(db.String(50))
    role = db.Column(db.String(80))
    gender = db.Column(db.String(10))
    date_of_birth = db.Column(db.DateTime())
    city = db.Column(db.String(80))
    university = db.Column(db.String(1024))
    specialization = db.Column(db.String(512))
    second_specialization = db.Column(db.String(512))
    image_icon = db.Column(db.String(512))
    uid = db.Column(db.String(128))

    def __init__(self, uid, email):
        self.email = email
        self.uid = uid


