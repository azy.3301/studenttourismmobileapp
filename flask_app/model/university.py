from flask_app import db


class University(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_university_id = db.Column(db.Integer)
    title = db.Column(db.String(80))
    description = db.Column(db.String(512))
    address = db.Column(db.String(512))
    image_main = db.Column(db.String(512))

