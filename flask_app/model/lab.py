from flask_app import db


class Lab(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    description = db.Column(db.String(1024))
    type = db.Column(db.String(50))
    link = db.Column(db.String(80))
    image_main = db.Column(db.String(80))
    image_gallery = db.Column(db.String(512))
