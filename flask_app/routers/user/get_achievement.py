from flask_app import app
from flask import request
import json


@app.route("/user/achievement/get", methods=["GET"])
def get_achievement():
    if request.method == "GET":
        headers = request.headers
        response = "{}"
    else:
        response = '{"message": "method not allowed}"'
    return json.loads(response)
