from flask_app import app
from flask import request
import json


@app.route("/user/delete", methods=["DELETE"])
def delete():
    if request.method == "DELETE":
        headers = request.headers
        response = "{}"
    else:
        response = '{"message": "method not allowed}"'
    return json.loads(response)