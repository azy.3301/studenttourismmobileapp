from flask_app import app
from flask import request
import json


@app.route("/user/trip/delete", methods=["DELETE"])
def delete_trip():
    if request.method == "DELETE":
        headers = request.headers
        response = "{}"
    else:
        response = '{"message": "method not allowed}"'
    return json.loads(response)
