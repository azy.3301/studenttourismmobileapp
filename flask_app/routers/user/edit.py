import datetime

from flask_app import app
from flask import request

from flask_app.model.user import User
from helpers import get_user
from flask_app import db
import json


@app.route("/user/edit", methods=["PUT"])
def edit_user():
    if request.method == "PUT":
        headers = request.headers
        user_data = get_user(headers["token"])
        user = User.query.filter_by(uid=user_data.get("uid")).first()
        if not user:
            new_user = User(user_data["uid"], user_data["email"])
            db.session.add(new_user)
            db.session.commit()
            response = '{"message": "created user '+str(new_user)+'"}'
        else:
            user.phone = headers.get("phone") or 0
            user.vk = headers.get("vk") or ""
            user.first_name = headers.get("first_name") or ""
            user.last_name = headers.get("last_name") or ""
            user.patronymic = headers.get("patronymic") or ""
            user.role = headers.get("role") or ""
            user.gender = headers.get("sex") or ""
            user.date_of_birth = headers.get("birthdate") or datetime.datetime.now()
            user.city = headers.get("city") or ""
            user.university = headers.get("university") or ""
            user.specialization = headers.get("spec") or ""
            user.second_specialization = headers.get("adit_spec") or ""
            user.image_icon = ""
            db.session.commit()
            response = '{"edited": 1}'
    else:
        response = '{"edited": 0}'
    return json.loads(response)
