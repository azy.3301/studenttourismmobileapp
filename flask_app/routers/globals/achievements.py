from flask_app import app
from flask import request
from flask_app.model.achievement import Achieve
import json


@app.route("/achievements/all", methods=["GET"])
def get_all_achievements():
    if request.method == "GET":
        achieves = Achieve.query.all()
        response = []
        for achieve in achieves:
            achieve: Achieve
            response.append({
                "id": achieve.id,
                "title": achieve.title,
                "description": achieve.description,
                "image_main": achieve.image_main,
                "score": achieve.score
            })
    else:
        response = {"message": "method not allowed"}
    return json.dumps(response)
