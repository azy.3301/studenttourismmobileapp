from flask_app import app
from flask import request
import json


@app.route("/universities/all", methods=["GET"])
def get_universities():
    if request.method == "GET":
        # logic
        response = "{}"
    else:
        response = '{"message": "method not allowed}"'
    return json.loads(response)


@app.route("/universities/<int:ident>", methods=["GET"])
def get_university_by_id(ident: int):
    if request.method == "GET":
        # logic
        response = "{}"
    else:
        response = '{"message": "method not allowed}"'
    return json.loads(response)
