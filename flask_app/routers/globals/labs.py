from flask_app import app
from flask import request
import json
from flask_app.model.lab import Lab


@app.route("/labs/all", methods=["GET"])
def get_labs():
    if request.method == "GET":
        labs = Lab.query.all()
        response = []
        for lab in labs:
            lab: Lab
            response.append({
                "title": lab.title,
                "id": lab.id,
                "image_main": lab.image_main,
                "description": lab.description,
                "type": lab.type,
                "image_gallery": lab.image_gallery,
                "link": lab.link,
            })
    else:
        response = {"message": "method not allowed}"}
    return json.dumps(response)


@app.route("/labs/<int:ident>", methods=["GET"])
def get_lab_by_id(ident: int):
    if request.method == "GET":
        lab = Lab.query.filter_by(id=ident).first()
        response = {
            "title": lab.title,
            "id": lab.id,
            "image_main": lab.image_main,
            "description": lab.description,
            "type": lab.type,
            "image_gallery": lab.image_gallery,
            "link": lab.link,
        }
    else:
        response = {"message": "method not allowed"}
    return json.dumps(response)
