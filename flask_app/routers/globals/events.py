from flask_app import app
from flask import request
import json
from flask_app.model.event import Event


@app.route("/events/all", methods=["GET"])
def get_events():
    if request.method == "GET":
        events = Event.query.all()
        js = []
        for event in events:
            event: Event
            js.append({
                "id": event.id,
                "contact_phone": event.contact_phone,
                "title": event.title,
                "description": event.description,
                "contact_email": event.contact_email,
                "contact_name": event.contact_name,
                "finish_date": event.finish_date.strftime("%d-%m-%Y"),
                "image_main": event.image_main,
                "start_date": event.start_date.strftime("%d-%m-%Y")
            })

    else:
        js = '{"message": "method not allowed}"'
    return json.dumps(js)


@app.route("/events/<int:ident>", methods=["GET"])
def get_event_by_id(ident: int):
    if request.method == "GET":
        event = Event.query.filter_by(id=ident).first()
        response = {
            "id": event.id,
            "contact_phone": event.contact_phone,
            "title": event.title,
            "description": event.description,
            "contact_email": event.contact_email,
            "contact_name": event.contact_name,
            "finish_date": event.finish_date.strftime("%d-%m-%Y"),
            "image_main": event.image_main,
            "start_date": event.start_date.strftime("%d-%m-%Y")
        }
    else:
        response = {"message": "method not allowed"}
    return json.dumps(response)
