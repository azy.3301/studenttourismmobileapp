from flask_app import app
from flask import request
from flask_app.model.room import Room
import json


@app.route("/rooms/all", methods=["GET"])
def get_all_rooms():
    if request.method == "GET":
        rooms = Room.query.all()
        response = []
        for room in rooms:
            room: Room
            response.append({
                "id": room.id,
                "title": room.title,
                "description": room.description,
                "number_residents": room.number_residents,
                "max_time_days": room.max_time_days,
                "min_time_days": room.min_time_days,
                "address": room.address,
                "food": room.food,
                "university_id": room.university_id,
                "price": room.price
            })
    else:
        response = {"message": "method not allowed"}
    return json.dumps(response)
