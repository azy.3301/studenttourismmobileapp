from flask_app import app
from flask import request
import json


@app.route("/news/all", methods=["GET"])
def get_news():
    if request.method == "GET":
        # logic
        response = "{}"
    else:
        response = {"message": "method not allowed"}
    return json.loads(response)


@app.route("/news/<int:ident>", methods=["GET"])
def get_new_by_id(ident: int):
    if request.method == "GET":
        # logic
        response = "{}"
    else:
        response = '{"message": "method not allowed}"'
    return json.loads(response)
